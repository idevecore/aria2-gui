/* utils.js
 *
 * Copyright 2023 Ideve Core
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import Gio from 'gi://Gio';

/**
 *
 * Create a instance of settings application
 * @param {object} params
 * @param {string} params.schema_id
 *
 * @returns {object}
 *
 */
const settings = (params) => {
  const gsettings = new Gio.Settings(params);

  /**
   *
   * Set a simple instring in application settings
   * @param {object} params
   * @param {string} params.type
   * @param {string} params.value
   *
   */
  const set_string = (params) => gsettings.set_string(params);

  /**
   *
   * bind values
   * @param {object} params
   * @param {string} params.key - the key to bind
   * @param {GObject.Object} params.object - a GObject
   * @param {string} params.property - the name of the property to bind
   * @param {Gio.SettingsBindFlags} params.flags -  flags for the binding
   *
   * @return {any}
   *
   */
  const bind = ({ key, object, property, flags }) => (gsettings.bind(key, object, property, flags));

  return {
    set_string,
    bind,
  }
}


/**
 *
 * Responsible for alls utils of application
 * @param {object} params
 * @param {Adw.Application} @params.application
 *
 * @typeref {object}
 * @property {settings} settings
 *
 */
export const utils = ({ application }) => {
  const settings_instance = settings({ schema_id: application.application_id });
  return {
    settings: settings_instance,
  }
}

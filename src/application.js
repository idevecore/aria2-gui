/* application.js
 *
 * Copyright 2023 Ideve Core
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import Adw from 'gi://Adw?version=1';
import Gio from 'gi://Gio';
import { Aria2GUIWindow } from './window.js';
import { utils } from './utils.js';

/**
 *
 * Create a instance of Libawaita Application
 *
 */
export const application = new Adw.Application({
  application_id: pkg.name,
  flags: Gio.ApplicationFlags.DEFAULT_FLAGS,
  resource_base_path: '/io/gitlab/idevecore/Aria2GUI',
});

/**
 *
 * The ::startup signal is emitted on the primary instance immediately after registration. See g_application_register().
 * See: https://docs.gtk.org/gio/signal.Application.startup.html
 * @param {Adw.Application} user_data
 *
 */
application.connect('startup', (user_data) => {
  user_data.utils = utils({ application: user_data });
});


/**
 *
 * The ::activate signal is emitted on the primary instance when an activation occurs. See g_application_activate().
 * See: https://docs.gtk.org/gio/signal.Application.activate.html
 * @param {Adw.Application} user_data
 *
 */
application.connect('activate', (user_data) => {
  Aria2GUIWindow({ application: user_data }).present();
});

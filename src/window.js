/* window.js
 *
 * Copyright 2023 Ideve Core
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';
import Adw from 'gi://Adw';
import Gio from 'gi://Gio';
import resource from './window.blp';

/**
 *
 * Create main window of application
 * @param {object} params
 * @param {Adw.Application} params.application
 *
 * @returns {Adw.applicationWindow}
 *
 */
export const Aria2GUIWindow = ({ application }) => {
  const builder = Gtk.Builder.new_from_resource(resource);
  const settings = application.utils.settings;

  const window = builder.get_object('window');

  if(__devel__) {
    window.add_css_class('devel');
  }

  window.set_application(application);
  load_window_state({ window, settings });
  return window;
}


/**
 *
 * Load window states from application settings
 *
 * @param {object} params
 * @param {Adw.ApplicationWindow} params.window
 * @param {object} settings
 *
 */
const load_window_state = ({ window, settings }) => {
  settings.bind(
    {
      key: 'width',
      object: window,
      property: 'default-width',
      flags: Gio.SettingsBindFlags.DEFAULT,
    });
  settings.bind({
    key: 'height',
    object: window,
    property: 'default-height',
    flags: Gio.SettingsBindFlags.DEFAULT,
  });
  settings.bind({
    key: 'is-maximized',
    object: window,
    property: 'maximized',
    flags: Gio.SettingsBindFlags.DEFAULT,
  });
  settings.bind({
    key: 'is-fullscreen',
    object: window,
    property: 'fullscreened',
    flags: Gio.SettingsBindFlags.DEFAULT,
  });
}
